//
//  ViewController.m
//  Recipe
//
//  Created by Zachary Cole on 2/5/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    
    array = [[NSArray alloc] initWithContentsOfFile:path];
    
    myTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:myTableView];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cell"];
    }
    
    NSDictionary* dictionary = array[indexPath.row];
    
    NSString* stringToDisplay = dictionary[@"title"];
    
    cell.textLabel.text = stringToDisplay;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    DetailViewController* dvc = [DetailViewController new];

    NSDictionary* dictionary = array[indexPath.row];
    
    dvc.infoDictionary = dictionary;

    [self.navigationController pushViewController:dvc animated:YES];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
