//
//  DetailViewController.h
//  Recipe
//
//  Created by Zachary Cole on 2/6/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary* infoDictionary;

@end
