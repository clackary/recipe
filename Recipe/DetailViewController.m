//
//  DetailViewController.m
//  Recipe
//
//  Created by Zachary Cole on 2/6/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    iv.image = [UIImage imageNamed:self.infoDictionary[@"image"]];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:iv];
    
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(100, 200, 250, 50)];
    [label setText:self.infoDictionary[@"description"]];
    [self.view addSubview:label];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
