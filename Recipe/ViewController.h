//
//  ViewController.h
//  Recipe
//
//  Created by Zachary Cole on 2/5/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    NSArray* array;
}

@end

