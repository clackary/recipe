//
//  AppDelegate.h
//  Recipe
//
//  Created by Zachary Cole on 2/5/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

